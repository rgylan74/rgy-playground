# Build and run as a Standalone Spring Boot App
```
// Mac/Linux
$ ./gradlew build && ./gradlew :bootRun
// Windows
> gradlew build && gradlew :bootRun
```
# Sample response payload for GET /weather-info/by-cities
{
    "responseId": "875034fc5b2e3f709e9b233fc16245b8",
    "response": [
        {
            "location": "LONDON",
            "actualWeather": "light rain / ",
            "temperature": "5.66"
        },
        {
            "location": "PRAGUE",
            "actualWeather": "overcast clouds / ",
            "temperature": "1.16"
        },
        {
            "location": "SAN FRANCISCO",
            "actualWeather": "clear sky / ",
            "temperature": "14.16"
        }
    ],
    "statusCode": "200",
    "message": "General success message."
}

# Sample request payload for POST /weather-info/by-cities
{
    "requestId": "875034fc5b2e3f709e9b233fc16245b8",
    "request": [
        {
            "location": "LONDON",
            "actualWeather": "light rain / ",
            "temperature": "5.66"
        },
        {
            "location": "PRAGUE",
            "actualWeather": "overcast clouds / ",
            "temperature": "1.16"
        },
        {
            "location": "SAN FRANCISCO",
            "actualWeather": "clear sky / ",
            "temperature": "14.16"
        }
    ]
}
