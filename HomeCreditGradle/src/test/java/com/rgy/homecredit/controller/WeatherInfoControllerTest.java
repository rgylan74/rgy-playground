package com.rgy.homecredit.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rgy.homecredit.delegate.ResultsWrapper;
import com.rgy.homecredit.delegate.WeatherInfoDelegate;
import com.rgy.homecredit.dto.WeatherInfoDTO;
import com.rgy.homecredit.util.WeatherInfoEnum;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class WeatherInfoControllerTest {

    private MockMvc mvc;
    private ObjectMapper mapper;

    private WeatherInfoDelegate delegate;
    private WeatherInfoController controller;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mapper = new ObjectMapper();
        delegate = mock(WeatherInfoDelegate.class);
        controller = new WeatherInfoController(delegate);
        mvc = MockMvcBuilders.standaloneSetup(controller).build();

    }

    @Test
    public void getWeatherInfoByCityIds() throws Exception {
        List<WeatherInfoDTO> list = new ArrayList<>();
        WeatherInfoDTO dto1 = new WeatherInfoDTO("LONDON", "shower rain / ", "5", "12345");
        list.add(dto1);

        given(delegate.delegateWeatherInfoByCityIds(anyString(), anyString()))
                .willReturn(new ResultsWrapper<>(list, WeatherInfoEnum.SUCCESS.getCode(), WeatherInfoEnum.SUCCESS.getMessage()));

        mvc.perform(defaultGetRequestBuilderWithJsonBody("/weather-info/by-cities?id=12345&units=metric",
                jsonToString(new WeatherInfoDTO())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().json(
                        "{\n" +
                                "    \"responseId\": \"377eb9675d22332d924b524d6bcf9718\",\n" +
                                "    \"response\": [\n" +
                                "        {\n" +
                                "            \"location\": \"LONDON\",\n" +
                                "            \"actualWeather\": \"shower rain / \",\n" +
                                "            \"temperature\": \"5\"\n" +
                                "        }\n" +
                                "    ],\n" +
                                "    \"statusCode\": \"200\",\n" +
                                "    \"message\": \"General success message.\"\n" +
                                "}", true));
    }

    public static MockHttpServletRequestBuilder defaultRequestBuilderWithJsonBody(HttpMethod method, String path) throws Exception {

        return MockMvcRequestBuilders
                .request(method, new URI(path))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);
    }

    public static MockHttpServletRequestBuilder defaultPostRequestBuilderWithJsonBody(String path, String body) throws Exception {
        return defaultRequestBuilderWithJsonBody(HttpMethod.POST, path)
                .content(body);
    }

    public static MockHttpServletRequestBuilder defaultGetRequestBuilderWithJsonBody(String path, String body) throws Exception {
        return defaultRequestBuilderWithJsonBody(HttpMethod.GET, path)
                .content(body);
    }

    private String jsonToString(Object o) throws Exception {
        return mapper.writeValueAsString(o);
    }
}
