package com.rgy.homecredit.exception;

public class WeatherInfoException extends Exception {

    public WeatherInfoException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
