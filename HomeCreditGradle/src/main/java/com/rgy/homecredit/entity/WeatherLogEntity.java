package com.rgy.homecredit.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "WeatherLog")
public class WeatherLogEntity extends AbstractEntity {

    @Column(name = "responseId", nullable = false)
    private String responseId;

    @Column(name = "location", nullable = false)
    private String location;

    @Column(name = "actualWeather", nullable = false)
    private String actualWeather;

    @Column(name = "temperature", nullable = false)
    private String temperature;

    public String getResponseId() {
        return responseId;
    }

    public void setResponseId(String responseId) {
        this.responseId = responseId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getActualWeather() {
        return actualWeather;
    }

    public void setActualWeather(String actualWeather) {
        this.actualWeather = actualWeather;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }
}
