package com.rgy.homecredit.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.time.LocalDateTime;

@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

    @GenericGenerator(
            name = "WeatherInfoSeqGen",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = "sequence_name", value = "WEATHER_INFO_SEQUENCE"),
                    @org.hibernate.annotations.Parameter(name = "initial_value", value = "1000"),
                    @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")
            }
    )
    @GeneratedValue(generator = "WeatherInfoSeqGen")
    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    protected Long id;

    @Column(name = "dtimeInserted", nullable = false)
    protected LocalDateTime dtimeInserted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDtimeInserted() {
        return dtimeInserted;
    }

    public void setDtimeInserted(LocalDateTime dtimeInserted) {
        this.dtimeInserted = dtimeInserted;
    }
}
