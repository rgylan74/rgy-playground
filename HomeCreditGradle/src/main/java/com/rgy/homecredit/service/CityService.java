package com.rgy.homecredit.service;

import com.rgy.homecredit.exception.WeatherInfoException;

/**
 * This is a Service Class that manages list of cities.
 * The cities can be stored in a database or in a json file.
 *
 */
public interface CityService {

    /**
     * Get the city id given country code (2 Character code) and city name.
     *
     * @param countryCode - The 2 character country code.
     * @param cityName - The city name.
     * @return - The city id if found otherwise null.
     * @throws WeatherInfoException - If there is parsing error or database access error.
     */
    String getCityId(String countryCode, String cityName) throws WeatherInfoException;
}
