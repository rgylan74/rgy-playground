package com.rgy.homecredit.service;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.rgy.homecredit.config.WeatherInfoProperty;
import com.rgy.homecredit.exception.WeatherInfoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileInputStream;

@Service("FileCityServiceImpl")
public class FileCityServiceImpl implements CityService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass().getName());

    private final WeatherInfoProperty property;

    @Autowired
    public FileCityServiceImpl(WeatherInfoProperty property) {
        this.property = property;
    }

    @Override
    public String getCityId(String countryCode, String cityName) throws WeatherInfoException {
        String cityId = null;
        JsonParser parser = null;
        try {
            JsonFactory jsonfactory = new JsonFactory();
            File file = ResourceUtils.getFile(property.getCityFile());
            parser = jsonfactory.createParser(new FileInputStream(file));

            boolean foundCountryCode = false;
            boolean foundCityName = false;
            while (parser.nextToken() != JsonToken.END_ARRAY) {
                String token = parser.getCurrentName();
                if ("id".equals(token)) {
                    parser.nextToken();
                    cityId = parser.getText();
                }
                if ("name".equals(token)) {
                    foundCityName = false;
                    parser.nextToken();
                    if (cityName.equalsIgnoreCase(parser.getText())) {
                        foundCityName = true;
                    }
                }
                if ("country".equals(token)) {
                    foundCountryCode = false;
                    parser.nextToken();
                    if (countryCode.equalsIgnoreCase(parser.getText())) {
                        foundCountryCode = true;
                    }
                }
                if (foundCountryCode && foundCityName) {
                    LOGGER.info("Found matching country code and city name.");
                    return cityId;
                }
            }

        } catch (Exception e) {
            throw new WeatherInfoException("Could not parse city file: " + e.getMessage(), e);
        } finally {
            try {
                parser.close();
            } catch (Exception e) {
                throw new WeatherInfoException("Could not close json file: " + e.getMessage(), e);
            }
        }

        return cityId;
    }
}
