package com.rgy.homecredit.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rgy.homecredit.client.OWMClient;
import com.rgy.homecredit.dto.WeatherInfoDTO;
import com.rgy.homecredit.entity.WeatherLogEntity;
import com.rgy.homecredit.exception.WeatherInfoException;
import com.rgy.homecredit.repository.WeatherLogRepository;
import com.rgy.homecredit.util.WeatherInfoUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class WeatherInfoServiceImpl {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass().getName());

    private static final String DEFAULT_CITIES = "2643743,3067696,5391959";
    private static final String DEFAULT_UNIT = "metric";
    private static final String[] DEFAULT_CITY_NAME = {"LONDON", "PRAGUE", "SAN FRANCISCO"};

    private OWMClient owmClient;
    private WeatherLogRepository repository;

    @Autowired
    public WeatherInfoServiceImpl(OWMClient owmClient, WeatherLogRepository repository) {
        this.owmClient = owmClient;
        this.repository = repository;
    }

    public List<WeatherInfoDTO> getWeatherInfoByCityIds(String cityIds, String units) throws WeatherInfoException {
        if (ObjectUtils.isEmpty(cityIds)) {
            // Use default cities(London, Prague, San Francisco)
            cityIds = DEFAULT_CITIES;
        }
        if (ObjectUtils.isEmpty(units)) {
            // Use default unit, metric
            units = DEFAULT_UNIT;
        }

        String json = owmClient.getWeatherInfoByCityIds(cityIds, units).getResponse();
        ObjectMapper mapper = new ObjectMapper();
        List<WeatherInfoDTO> list = null;

        try {
            JsonNode root = mapper.readTree(json);
            JsonNode listNode = root.path("list");
            if (!listNode.isMissingNode() && listNode.isArray()) {
                list = new ArrayList<>();
                for (JsonNode lNode : listNode) {
                    String location = lNode.path("name").asText().toUpperCase();
                    String dt = lNode.path("dt").asText();
                    JsonNode weatherNode = lNode.path("weather");
                    StringBuilder sb = new StringBuilder();
                    for (JsonNode wNode : weatherNode) {
                        sb.append(wNode.path("description").asText());
                        sb.append(" / ");
                    }
                    String temperature = lNode.path("main").path("temp").asText();

                    WeatherInfoDTO dto = new WeatherInfoDTO(location, sb.toString(), temperature, dt);
                    list.add(dto);
                }
            }

        } catch (Exception e) {
            throw new WeatherInfoException("Could not get weather info by city Ids: " + e.getMessage(), e);
        }

        return list;
    }

    @Transactional
    public void saveLatest5WeatherInfo(String requestId, List<WeatherInfoDTO> dtos) {
        if (ObjectUtils.isEmpty(repository.findByResponseId(requestId))) {
            delete5thElementFromTheList();
            for (WeatherInfoDTO dto : dtos) {
                WeatherLogEntity log = new WeatherLogEntity();
                log.setActualWeather(dto.getActualWeather());
                log.setLocation(dto.getLocation());
                log.setTemperature(dto.getTemperature());
                log.setResponseId(requestId);
                log.setDtimeInserted(LocalDateTime.now());

                repository.save(log);
                LOGGER.info("Successfully saved latest weather info.");
            }
        }

    }

    private void delete5thElementFromTheList() {
        for (String city : DEFAULT_CITY_NAME) {
            List<WeatherLogEntity> list = repository.findByLocationOrderByIdDesc(city);
            if (!ObjectUtils.isEmpty(list) && list.size() == 5) {
                repository.delete(list.get(list.size() - 1).getId());
                LOGGER.info("Successfully deleted 5th {} weather info.", city);
            }
        }
    }

}
