package com.rgy.homecredit.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Objects;

public class WeatherInfoDTO implements Serializable {

    private static final long serialVersionUID = -6137294652834671508L;

    private String location;
    private String actualWeather;
    private String temperature;
    @JsonIgnore
    private String dt;

    public WeatherInfoDTO(){}

    public WeatherInfoDTO(String location, String actualWeather, String temperature, String dt) {
        this.location = location;
        this.actualWeather = actualWeather;
        this.temperature = temperature;
        this.dt = dt;
    }

    public String getLocation() {
        return location;
    }

    public String getActualWeather() {
        return actualWeather;
    }

    public String getTemperature() {
        return temperature;
    }

    public String getDt() {
        return dt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WeatherInfoDTO that = (WeatherInfoDTO) o;
        return Objects.equals(location, that.location) &&
                Objects.equals(actualWeather, that.actualWeather) &&
                Objects.equals(temperature, that.temperature) &&
                Objects.equals(dt, that.dt);
    }

    @Override
    public int hashCode() {

        return Objects.hash(location, actualWeather, temperature, dt);
    }

    @Override
    public String toString() {
        return "WeatherInfoDTO{" +
                "location='" + location + '\'' +
                ", actualWeather='" + actualWeather + '\'' +
                ", temperature='" + temperature + '\'' +
                ", dt='" + dt + '\'' +
                '}';
    }
}
