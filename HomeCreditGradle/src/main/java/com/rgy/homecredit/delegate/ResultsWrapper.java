package com.rgy.homecredit.delegate;

import java.io.Serializable;
import java.util.Objects;

public class ResultsWrapper<R> implements Serializable {

    private static final long serialVersionUID = 3384526711381748083L;

    private R result;
    private String messageCode;
    private String message;

    public ResultsWrapper(String messageCode, String message) {
        this.messageCode = messageCode;
        this.message = message;
    }

    public ResultsWrapper(R result, String messageCode, String message) {
        this.result = result;
        this.messageCode = messageCode;
        this.message = message;
    }

    public R getResult() {
        return result;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResultsWrapper<?> that = (ResultsWrapper<?>) o;
        return Objects.equals(result, that.result) &&
                Objects.equals(messageCode, that.messageCode) &&
                Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {

        return Objects.hash(result, messageCode, message);
    }

    @Override
    public String toString() {
        return "ResultsWrapper{" +
                "result=" + result +
                ", messageCode='" + messageCode + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
