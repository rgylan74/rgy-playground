package com.rgy.homecredit.delegate;

import com.rgy.homecredit.dto.WeatherInfoDTO;
import com.rgy.homecredit.service.WeatherInfoServiceImpl;
import com.rgy.homecredit.util.WeatherInfoEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Component
public class WeatherInfoDelegate {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass().getName());

    private final WeatherInfoServiceImpl weatherInfoService;

    public WeatherInfoDelegate(WeatherInfoServiceImpl weatherInfoService) {
        this.weatherInfoService = weatherInfoService;
    }

    public ResultsWrapper<List<WeatherInfoDTO>> delegateWeatherInfoByCityIds(String cityIds, String units) {
        try {
            // Return success results.
            return new ResultsWrapper<>(weatherInfoService.getWeatherInfoByCityIds(cityIds, units),
                    WeatherInfoEnum.SUCCESS.getCode(), WeatherInfoEnum.SUCCESS.getMessage());

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return returnBasicFailResults();
    }

    public ResultsWrapper<String> delegatePostWeatherInfo(String requestId, List<WeatherInfoDTO> request) {
        try {
            if (!ObjectUtils.isEmpty(request)) {
                weatherInfoService.saveLatest5WeatherInfo(requestId, request);
                return returnBasicSuccessResults();
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return returnBasicFailResults();
    }

    private ResultsWrapper returnBasicFailResults() {
        return new ResultsWrapper<>(WeatherInfoEnum.FAIL.getCode(), WeatherInfoEnum.FAIL.getMessage());
    }

    private ResultsWrapper returnBasicSuccessResults() {
        return new ResultsWrapper<>(WeatherInfoEnum.SUCCESS.getCode(), WeatherInfoEnum.SUCCESS.getMessage());
    }
}
