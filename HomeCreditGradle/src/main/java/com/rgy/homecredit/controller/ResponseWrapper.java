package com.rgy.homecredit.controller;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseWrapper<P> implements Serializable {

    private static final long serialVersionUID = -5108154487840141223L;

    private String responseId;
    private P response;
    private String statusCode;
    private String message;

    public ResponseWrapper(P response) {
        this.response = response;
    }

    public ResponseWrapper(String statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
    }

    public ResponseWrapper(P response, String statusCode, String message) {
        this.response = response;
        this.statusCode = statusCode;
        this.message = message;
    }

    public ResponseWrapper(String responseId, P response, String statusCode, String message) {
        this.responseId = responseId;
        this.response = response;
        this.statusCode = statusCode;
        this.message = message;
    }

    public String getResponseId() {
        return responseId;
    }

    public P getResponse() {
        return response;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResponseWrapper<?> that = (ResponseWrapper<?>) o;
        return Objects.equals(responseId, that.responseId) &&
                Objects.equals(response, that.response) &&
                Objects.equals(statusCode, that.statusCode) &&
                Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {

        return Objects.hash(responseId, response, statusCode, message);
    }

    @Override
    public String toString() {
        return "ResponseWrapper{" +
                "responseId='" + responseId + '\'' +
                ", response=" + response +
                ", statusCode='" + statusCode + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
