package com.rgy.homecredit.controller;

import com.rgy.homecredit.exception.WeatherInfoException;
import com.rgy.homecredit.service.CityService;
import com.rgy.homecredit.util.WeatherInfoEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin")
public class AdminController {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass().getName());

    private final CityService cityService;

    @Autowired
    public AdminController(@Qualifier("FileCityServiceImpl") CityService cityService) {
        this.cityService = cityService;
    }

    @GetMapping(path = "/file/{countrycode}/{city}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper<String>> getCity(@PathVariable("countrycode") String countryCode,
                                                           @PathVariable("city") String city) {
        String cityId = null;
        try {
            LOGGER.info("Country code: {}, city: {}", countryCode, city);
            cityId = cityService.getCityId(countryCode, city);
        } catch (WeatherInfoException e) {
            LOGGER.error("Could not get city id for given country {} and city {}.  Exception: {}", countryCode, city, e);
            return ResponseEntity.ok(new ResponseWrapper<String>("Could not get city id.",
                    HttpStatus.INTERNAL_SERVER_ERROR.toString(), WeatherInfoEnum.FAIL.getMessage()));
        }
        return ResponseEntity.ok(new ResponseWrapper<String>(cityId, HttpStatus.OK.toString(), WeatherInfoEnum.SUCCESS.getMessage()));
    }
}
