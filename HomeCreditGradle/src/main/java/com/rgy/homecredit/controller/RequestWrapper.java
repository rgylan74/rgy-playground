package com.rgy.homecredit.controller;

import java.io.Serializable;
import java.util.Objects;

public class RequestWrapper<P> implements Serializable {

    private static final long serialVersionUID = -1025487945424309697L;

    private String requestId;
    private P request;

    public RequestWrapper() {
    }

    public RequestWrapper(String requestId, P request) {
        this.requestId = requestId;
        this.request = request;
    }

    public String getRequestId() {
        return requestId;
    }

    public P getRequest() {
        return request;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RequestWrapper<?> that = (RequestWrapper<?>) o;
        return Objects.equals(requestId, that.requestId) &&
                Objects.equals(request, that.request);
    }

    @Override
    public int hashCode() {

        return Objects.hash(requestId, request);
    }

    @Override
    public String toString() {
        return "RequestWrapper{" +
                "requestId='" + requestId + '\'' +
                ", request=" + request +
                '}';
    }
}
