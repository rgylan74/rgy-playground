package com.rgy.homecredit.controller;

import com.rgy.homecredit.delegate.ResultsWrapper;
import com.rgy.homecredit.delegate.WeatherInfoDelegate;
import com.rgy.homecredit.dto.WeatherInfoDTO;
import com.rgy.homecredit.util.WeatherInfoUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/weather-info")
public class WeatherInfoController {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass().getName());

    private WeatherInfoDelegate weatherInfoDelegate;

    @Autowired
    public WeatherInfoController(WeatherInfoDelegate weatherInfoDelegate) {
        this.weatherInfoDelegate = weatherInfoDelegate;
    }

    /**
     * API to get weather information given a list of city Ids separated by comma.
     *
     * @param cityIds - List of city ids separated by comma ie 1122,3344,5566 defaults to LONDON, PRAGUE
     *                and SAN FRANCISCO if no Ids are present.
     * @param units - The unit of temperature, defaults to metric.
     * @return - Pls see README for sample response payload.
     *
     */
    @GetMapping(path = "/by-cities", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper<List<WeatherInfoDTO>>> getWeatherInfoByCityIds(@RequestParam("id") String cityIds,
                                                                                         @RequestParam("units") String units) {
        ResultsWrapper<List<WeatherInfoDTO>> result = weatherInfoDelegate.delegateWeatherInfoByCityIds(cityIds, units);
        return ResponseEntity.ok(new ResponseWrapper<List<WeatherInfoDTO>>(WeatherInfoUtil.generateSimpleUUID(result.getResult()),
                result.getResult(), WeatherInfoUtil.getStatusCode(result.getMessageCode()), result.getMessage()));
    }

    /**
     * API to save response payload from above API.
     *
     * @param payload - The request payload as per above response.
     * @return - Pls see README for sample request payload.
     */
    @PostMapping(path = "/by-cities", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseWrapper<String>> postWeatherInfo(@RequestBody RequestWrapper<List<WeatherInfoDTO>> payload) {
        LOGGER.info("Post weather info payload: {}", payload);
        ResultsWrapper<String> result = weatherInfoDelegate.delegatePostWeatherInfo(payload.getRequestId(), payload.getRequest());

        return ResponseEntity.ok(new ResponseWrapper<String>(WeatherInfoUtil.getStatusCode(result.getMessageCode()), result.getMessage()));
    }
}
