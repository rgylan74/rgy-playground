package com.rgy.homecredit.repository;

import com.rgy.homecredit.entity.WeatherLogEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WeatherLogRepository extends JpaRepository<WeatherLogEntity, Long> {

    List<WeatherLogEntity> findByLocationOrderByIdDesc(String location);

    List<WeatherLogEntity> findByResponseId(String responseId);
}
