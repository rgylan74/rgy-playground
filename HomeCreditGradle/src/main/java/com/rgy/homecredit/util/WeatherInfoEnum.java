package com.rgy.homecredit.util;

public enum WeatherInfoEnum {

    SUCCESS("SUCCESS", "General success message."),
    FAIL("FAILED", "General failed message");

    private String code;
    private String message;

    WeatherInfoEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
