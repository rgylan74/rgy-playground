package com.rgy.homecredit.util;

import com.rgy.homecredit.dto.WeatherInfoDTO;
import org.springframework.http.HttpStatus;
import org.springframework.util.ObjectUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public class WeatherInfoUtil {

    public static String getStatusCode(String code) {
        if (WeatherInfoEnum.SUCCESS.getCode().equals(code)) {
            return HttpStatus.OK.toString();
        }
        return HttpStatus.INTERNAL_SERVER_ERROR.toString();
    }

    public static String generateSimpleUUID(List<WeatherInfoDTO> infoList) {
        if (ObjectUtils.isEmpty(infoList)) {
            return UUID.nameUUIDFromBytes(LocalDateTime.now().toString().getBytes()).toString().replace("-", "");
        }
        StringBuilder sb = new StringBuilder();
        for (WeatherInfoDTO info : infoList) {
            sb.append(info.getActualWeather());
            sb.append(info.getDt());
            sb.append(info.getLocation());
            sb.append(info.getTemperature());
        }
        return UUID.nameUUIDFromBytes(sb.toString().getBytes()).toString().replace("-", "");
    }

}
