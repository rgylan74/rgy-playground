package com.rgy.homecredit.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("weather")
public class WeatherInfoProperty {

    private String apiKey;
    private String cityFile;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getCityFile() {
        return cityFile;
    }

    public void setCityFile(String cityFile) {
        this.cityFile = cityFile;
    }
}
