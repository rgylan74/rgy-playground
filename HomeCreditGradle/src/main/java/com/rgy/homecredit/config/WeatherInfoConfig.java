package com.rgy.homecredit.config;

import com.rgy.homecredit.client.OWMClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class WeatherInfoConfig {

    private final WeatherInfoProperty property;

    @Autowired
    public WeatherInfoConfig(WeatherInfoProperty property) {
        this.property = property;
    }

    @Bean
    public OWMClient owmClient() {
        OWMClient owmClient = new OWMClient(property);

        return owmClient;
    }
}
