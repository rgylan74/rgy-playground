package com.rgy.homecredit.client;

import com.rgy.homecredit.config.WeatherInfoProperty;
import com.rgy.homecredit.controller.ResponseWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

public class OWMClient extends ApiClient {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass().getName());

    private final static String OWM_BY_CITIES = "http://api.openweathermap.org/data/2.5/group?id=%s&units=%s&appid=%s";

    private WeatherInfoProperty property;

    public OWMClient(WeatherInfoProperty property) {
        this.property = property;
    }

    public ResponseWrapper<String> getWeatherInfoByCityIds(String cityIds, String unit) {
        String url = String.format(OWM_BY_CITIES, cityIds, unit, property.getApiKey());
        LOGGER.info("URL: {}", url);
        ResponseEntity<String> response = getRestTemplate().exchange(url,
                HttpMethod.GET, new HttpEntity<String>(new HttpHeaders()),
                new ParameterizedTypeReference<String>() {
                });

        return new ResponseWrapper(response.getBody());
    }
}
