package com.rgy.homecredit.client;

import org.springframework.web.client.RestTemplate;

public abstract class ApiClient {

    private RestTemplate restTemplate;

    public ApiClient() {
        this.restTemplate = new RestTemplate();
    }

    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

}
